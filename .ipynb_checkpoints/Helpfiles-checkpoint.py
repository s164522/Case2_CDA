import numpy as np
import pandas as pd
from scipy.stats import norm
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.utils.validation import check_is_fitted
from sklearn.impute import SimpleImputer


class SVDImputer(BaseEstimator, TransformerMixin):
    def __init__(self, num_components = 10):
        self.PCAbasis = None
        self.num_components = num_components

    def transform(self, X, y=None):
        return SVDcomplete_imputation_method(X, PCAbasis = self.PCAbasis)

    def fit(self, X, y=None):
        self.PCAbasis = SVDcomplete_imputation_method(X, num_components=self.num_components)
        return self


class RemoveNANsColumns(BaseEstimator, TransformerMixin):
    def __init__(self, NApercent = 0.3):
        self.NApercent = NApercent
        self.columns_removed = []

    def transform(self, X, y=None):
        return X.drop(columns = self.columns_removed)

    def fit(self, X, y=None):

        columns_removed = []
        for col in X.columns:
            if(X[col].isna().sum(axis=0) > len(X)*self.NApercent):
                columns_removed.append(col)
                
        self.columns_removed = columns_removed
        return self

class RemoveNANsRows(BaseEstimator, TransformerMixin):
    def __init__(self, NApercent = 0.25):
        self.NApercent = NApercent
        self.rows_removed = []

    def transform(self, X, y=None):
        print("Problems here? in RemoveNANsRows")
        rows_removed = self.rows_removed #Might not be nessesary?
        for row in X.iterrows():
            #print(row[0])
            if row[1].isna().sum() >= X.shape[1]*self.NApercent:
                rows_removed.append(row[0])
        
        keep_rows = np.setdiff1d(X.index.values, rows_removed)
        return X.loc[keep_rows]

    def fit(self, X, y=None):

        rows_removed = []
        for row in X.iterrows():
            #print(row[0])
            if row[1].isna().sum() >= X.shape[1]*self.NApercent:
                rows_removed.append(row[0])

        self.rows_removed = rows_removed
        return self

class ModeImputer(BaseEstimator, TransformerMixin):
    def __init__(self):
        self.strategy = 'most frequent'

    def transform(self, X, y=None):
        X_imputed = SimpleImputer(missing_values=np.nan, strategy=self.strategy)
        X_imputed = X_imputed.fit_transform(X)
        return X_imputed

    def fit(self, X, y=None):
        return self

class Binarize_transformer(BaseEstimator, TransformerMixin):
    def __init__(self):
        self.hej = 0

    def transform(self, X, y=None):
        var_types, _, _, _ = detect_variable_type(X, verbose=False)
        X_imputed = binarize_df(X, var_types)
        return X_imputed

    def fit(self, X, y=None):

        return self

class GroupImputer(BaseEstimator, TransformerMixin):
    '''
    Class used for imputing missing values in a pd.DataFrame using either mean or median of a group.
    
    Parameters
    ----------    
    group_cols : list
        List of columns used for calculating the aggregated value 
    target : str
        The name of the column to impute
    metric : str
        The metric to be used for remplacement, can be one of ['mean', 'median']

    Returns
    -------
    X : array-like
        The array with imputed values in the target column
    '''
    def __init__(self, group_cols, target, metric='mean'):
        
        assert metric in ['mean', 'median'], 'Unrecognized value for metric, should be mean/median'
        assert type(group_cols) == list, 'group_cols should be a list of columns'
        assert type(target) == str, 'target should be a string'
        
        self.group_cols = group_cols
        self.target = target
        self.metric = metric
    
    def fit(self, X, y=None):
        
        assert pd.isnull(X[self.group_cols]).any(axis=None) == False, 'There are missing values in group_cols'
        
        X_complete = X.dropna()
        
        impute_map = X.groupby(self.group_cols)[self.target].agg(self.metric) \
                                                            .reset_index(drop=False)
        
        self.impute_map_ = impute_map
        
        return self 
    
    def transform(self, X, y=None):
        
        # make sure that the imputer was fitted
        check_is_fitted(self, 'impute_map_')
        
        X = X.copy()
        
        for index, row in self.impute_map_.iterrows():
            ind = (X[self.group_cols] == row[self.group_cols]).all(axis=1)
            X.loc[ind, self.target] = X.loc[ind, self.target].fillna(row[self.target])
        
        return X.values

def detect_variable_type(df,Max_Number_Of_Ordinal_Values=10,verbose=True):
    # classification of variables into continuous, binary, ordinal
    # let us assume for the moment that all categorical variables have been converted to the (transformed?) complete disjunctive table
    # hence they will be represented as sets of binary variables
    
    binary = []
    continuous = []
    ordinal = []
    variable_types = []

    for col in df.columns: 
        vals = np.sort(df[col].unique())
        #print(col,vals)
        vals = [x for x in vals if str(x) != 'nan']
        tp = 'UNKNOWN'
        # NaNs must be 
        if len(vals)==2 or len(vals)==1:
            tp = 'BINARY'
            binary.append(col)
        if len(vals)>Max_Number_Of_Ordinal_Values:
            tp = 'CONTINUOUS'
            continuous.append(col)
        if len(vals)>2 and len(vals)<=Max_Number_Of_Ordinal_Values:
            tp = 'ORDINAL'
            ordinal.append(col)
        variable_types.append(tp)
        if len(vals)>Max_Number_Of_Ordinal_Values:
            if verbose:
                print(col,'\t','\t[',np.min(vals),'... ',len(vals),'values...',np.max(vals),']\t',tp)
        else:
            if verbose:
                print(col,'\t',vals,'\t',len(vals),'\t',tp)
    return variable_types, binary, continuous, ordinal

def SVDcomplete_imputation_method(dfq,PCAbasis = None,num_components=-1):
    verbose=False
    #print(type(PCAbasis))
    if(PCAbasis is not None):
        num_components = len(PCAbasis)
    variable_types,binary, continuous, ordinal = detect_variable_type(dfq,Max_Number_Of_Ordinal_Values=10,verbose=False)
    # IMPUTATION METHOD 'SVDcomplete'
    # dfq - quantified dataframe
    # imputation via computing SVD on complete matrix and then projecting vectors with missing values 
    # on the first num_components principal components
    # the only parameter of imputation - num_components - how many pca components to use
    # reasonable choice is the linear effective dimensionality

    dframe = dfq.dropna()
    dframe_mv = dfq
    dframe_mv.index = range(0,dframe_mv.shape[0]) #hack
    # we skip the first column - it is assumed to be the patient ids
    X = dframe[dframe.columns].to_numpy()

    if verbose:
        print('Matrix shape:',X.shape)
    
    # making pca on the complete matrix

    if(PCAbasis is None):
        if num_components<0:
            pca = PCA(n_components=min(X.shape))
            u = pca.fit_transform(X)
            v = pca.components_.T
            s = pca.explained_variance_ratio_
            sn = s/s[0]
            lin_dim = len(np.where(sn>0.1)[0])
            if verbose:
                print('Effective linear dimension',lin_dim)
            num_components = lin_dim
            
        pca = PCA(n_components=num_components)
        u = pca.fit_transform(X)
        PCAbasis = pca.components_.T
        s = pca.explained_variance_ratio_
        return PCAbasis
    else:
        # Extracting the matrix with missing values
        X_mv = dframe_mv[dframe_mv.columns].to_numpy()
        if verbose:
            print('Full matrix shape',X_mv.shape)

        # Centering the matrix with missing values
        Xmvc = X_mv - X.mean(axis=0,keepdims=True)

        # Projecting vectors with missing values on PC vectors
        u_mv = np.zeros((Xmvc.shape[0],PCAbasis.shape[1]))
        for i in range(0,Xmvc.shape[0]):
            xi = Xmvc[i,:]
            #if np.isnan(xi).sum()>0:
            #    print('x',str(i),'=',xi)
            for j in range(0,PCAbasis.shape[1]):
                vj = PCAbasis[:,j] #The j'th PCA componenet
                u_mv[i,j] = np.nansum(xi*vj) #Here is the magic/aka projection into the PCA component :-)
            #if np.isnan(xi).sum()>0:
            #    print('u_mv[i]=',u_mv[i,:])


        # Injecting the manifold back into the data space
        Xmvc_proj = np.transpose(np.matmul(PCAbasis,np.transpose(u_mv)))+X.mean(axis=0,keepdims=True) 
        
        # Now performing the actual value imputing. 
        # In case of ordinal and binary variables, we round each imputed 
        # value to the closest discrete value, otherwise we use the imputed value directly

        dfq_imputed = dframe_mv.copy()

        for i in range(1,dframe_mv.shape[1]+1):
            var_values = X_mv[:,i-1]
            var_values = var_values[np.where(~np.isnan(var_values))]
            var_values = np.unique(var_values)
            for j in dframe_mv.index:# range(0,dframe_mv.shape[0]):
                val = dframe_mv.loc[j,dframe_mv.columns[i-1]]
                #val = dframe_mv.iloc[j,i]
                if np.isnan(val):
                    val_imputed = Xmvc_proj[j,i-1]
                    #print('Imputed',str(j),str(i-1),str(val_imputed))
                    diffs = np.abs(var_values-val_imputed)
                    if variable_types[i-1]=='ORDINAL' or variable_types[i-1]=='BINARY':
                        val_imputed = var_values[np.argmin(diffs)]
                    #print(val_imputed,var_values,diffs)
                    dfq_imputed.loc[j,dfq_imputed.columns[i-1]] = val_imputed

    
        return dfq_imputed

def binarize_df(df, variable_type):
    df_copy = df.copy()
    df_cols = df.columns
    for i, var_type in enumerate(variable_type):
        col_name = df_cols[i]
        if col_name == "AGE":
            age_median = df["AGE"].median()
            df_copy["AGE_HIGH"] = (df["AGE"] >= age_median).astype(int)
            df_copy["AGE_LOW"] = (df["AGE"] < age_median).astype(int)
            df_copy = df_copy.drop("AGE", axis=1)
            continue
            
        if var_type == "ORDINAL":
            df_copy[col_name].fillna(df_copy[col_name].mode()[0], inplace=True)
            one_hot = pd.get_dummies(df_copy[col_name], prefix=col_name)
            df_copy = df_copy.drop(col_name, axis = 1)
            df_copy = df_copy.join(one_hot)
        elif var_type == "BINARY":            
            df_copy[col_name].fillna(df_copy[col_name].mode()[0], inplace=True)
            if len(np.unique(df_copy[col_name])) > 2:
                one_hot = pd.get_dummies(df_copy[col_name], prefix=col_name)
                df_copy = df_copy.drop(col_name, axis = 1)
                df_copy = df_copy.join(one_hot)           
            
        elif var_type == "CONTINUOUS":
            median_cont = df_copy[col_name].median()
            df_copy[col_name].fillna(median_cont, inplace=True)
            df_copy[col_name + "_HIGH"] = (df_copy[col_name] >= median_cont).astype(int)
            df_copy[col_name + "_LOW"] = (df_copy[col_name] < median_cont).astype(int)

    #print(cols)
    return df_copy